# Feature: Authentication
#   As a user
#   I want to be able to sign up and login
#   So that I can see my home screen

#   Scenario: Successful login

#     Given a set of specific users
#      | username     | first_name            | email                   | password |
#      | me           | Ioannis Oikonomidis   | s083928@student.dtu.dk  | 1234     |
#     When I log in as "me" "1234"
#     Then I should see "me" on the navigation bar

#   Scenario: Failed login

#     Given a set of specific users
#      | username     | first_name            | email                   | password |
#      | me           | Ioannis Oikonomidis   | s083928@student.dtu.dk  | 1234 |
#     When I log in as "me" "1235"
#     Then I should see a login error message

#   Scenario: Successful sign up

#     When I sign up as "me", "1234", "me@me.com"
#     Then I should see "me" on the navigation bar


#   Scenario: Failing sign up
# 	Given a set of specific users
#      | username    	| first_name 			      | email						        | password |
#      | me     		  | Ioannis Oikonomidis 	| s083928@student.dtu.dk 	| 1234 |
#     When I sign up as "me", "1234", "me@me.com"
#     Then I should see "This username is already taken. Please choose another."
